# Setup

> copy below files to gremlin server directory (~/apache-tinkerpop-gremlin-server-3.2.4)

1. conf/gremlin-server-dmango.yaml
2. conf/tinkergraph-dmango.properties
3. data/tinkerpop-dmango.json
4. scripts/load-dmango.groovy

# Start gremlin server

    bin/gremlin-server.sh conf/gremlin-server-dmango.yaml
